package ui;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import labTic.RestaurantMain;
import labTic.persistence.BookingRepository;
import labTic.persistence.TableRepository;
import labTic.services.RestaurantService;
import labTic.services.entities.Booking;
import labTic.services.entities.Tables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@Component
public class ReservaConfirmarController implements Initializable {

    @Autowired
    private RestaurantService restaurantService;

    private Booking booking;

    @FXML
    private Text txtNombre;

    @FXML
    private Text txtPersonas;

    @FXML
    private ChoiceBox<Long> selectorMesa;

    @FXML
    void btnConfirmar(MouseEvent event) {

       restaurantService.confirmBooking(booking,restaurantService.getTableById(booking.getRestaurant(),selectorMesa.getValue()));
        goToReservas(event);

    }

    private void goToReservas(Event event){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setControllerFactory((RestaurantMain.getContext()::getBean));

            Parent root = loader.load(ReservasController.class.getResourceAsStream("Restaurant/Reservas.fxml"));
            ReservasController controller = loader.getController();
            controller.setRestaurant(booking.getRestaurant());

            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();

            stage.setScene(new Scene(root));
            stage.getScene().getStylesheets().add(ReservasController.class.getResource("Restaurant/Reservas.css").toExternalForm());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @FXML
    void btnRechazar(MouseEvent event) {

        restaurantService.rejectBooking(booking);
        goToReservas(event);
    }

    @FXML
    void btnFiltroMultiple1(ContextMenuEvent event) {

    }

    @FXML
    void perfilTab(MouseEvent event) {

    }

    @FXML
    void reservasTab(MouseEvent event) {

    }

    void setBooking(Booking booking) {
        this.booking = booking;
        txtNombre.setText(booking.getAlias());
        txtPersonas.setText(booking.getAssistants().toString());

        List<Tables> mesas = restaurantService.getAvailableTables(booking.getRestaurant());

        for(Tables mesa : mesas) {
            selectorMesa.getItems().add(mesa.getId());
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {


    }

}
