package labTic.persistence;

import labTic.services.entities.Booking;
import labTic.services.entities.Restaurant;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookingRepository extends CrudRepository<Booking, Long> {

    Booking findByAlias(String alias);

    List<Booking> findAllByRut(Long rut);

    List<Booking> findAllByRestaurantAndConfirmedAndFinishedAndRejected(Restaurant restaurant, boolean confirmed, boolean finished, boolean rejected);

    Booking findByAliasAndFinished(String alias, boolean finished);

    Booking findByAliasAndRestaurantAndFinished(String alias, Restaurant restaurant, boolean finished);

    List<Booking> findByAliasAndConfirmedAndFinished(String alias, boolean confirmed, boolean finished);

    List<Booking> findAllByRestaurantAndFinishedAndConfirmed(Restaurant restaurant,boolean finished,boolean confirmed);

    Booking findByAliasAndFinishedAndRejected(String alias,boolean finished,boolean rejected);

}
