package labTic.services.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Table(name = "booking")
public class Booking {

    @Id
    @GeneratedValue(generator = "booking_ids")
    @GenericGenerator(name = "booking_ids", strategy = "increment")
    private long id;

    @ManyToOne
    @JoinColumn(name = "restaurant")
    private Restaurant restaurant;

    @Column(name = "booking_rut")
    private Long rut;

    @Column(name = "booking_alias")
    private String alias;

    @Column(name = "booking_assistants")
    private Integer assistants;

    @Column(name = "booking_confirmation")
    private Boolean confirmed;

    @Column(name = "booking_rejection")
    private Boolean rejected;

    @Column(name = "booking_finalization")
    private Boolean finished;

    @ManyToOne
    @JoinColumn(name = "tables_ids")
    private Tables table;

    public Booking(){}

    public void setTable(Tables table) {
        this.table = table;
    }

    public Booking(Restaurant restaurant, Integer assistants, String alias) {
        this.restaurant = restaurant;
        this.assistants = assistants;
        this.alias = alias;
        this.confirmed = false;
        //cambios
        this.finished = false;
        this.rejected = false;
    }

    public Booking(Restaurant restaurant, Integer assistants, String alias, Tables table) {
        this.restaurant = restaurant;
        this.assistants = assistants;
        this.alias = alias;
        this.table = table;
        this.confirmed = false;
        //cambios
        this.finished = false;
        this.rejected = false;
    }

    public Booking(Restaurant restaurant, String alias, Integer assistants){
        this.restaurant=restaurant;
        this.alias = alias;
        this.assistants = assistants;
        this.confirmed = false;
        this.rejected = false;
        this.finished = false;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setConfirmed() {
        this.confirmed = true;
    }

    public void setRejected() {
        this.rejected = true;
    }

    public void setFinished() {
        this.finished = true;
        this.restaurant.setCompletedReservations(this.restaurant.getCompletedReservations() + 1);
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }



    public String getAlias() {
        return alias;
    }

    public Integer getAssistants() {
        return assistants;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public Boolean getRejected() {
        return rejected;
    }

    public Boolean getFinished() {
        return finished;
    }

    public Tables getTable() {
        return table;
    }
}
