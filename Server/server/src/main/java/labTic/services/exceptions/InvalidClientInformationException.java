package labTic.services.exceptions;

public class InvalidClientInformationException extends Exception {

    public InvalidClientInformationException(String message) {
        super(message);
    }
}
