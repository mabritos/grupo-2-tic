package labTic.services.exceptions;

public class InvalidRestaurantInformationException extends Exception {

    public InvalidRestaurantInformationException(String message) {
        super(message);
    }
}
