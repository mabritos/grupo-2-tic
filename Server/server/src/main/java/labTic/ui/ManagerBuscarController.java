package labTic.ui;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import labTic.ManagerMain;
import labTic.persistence.RestaurantRepository;
import labTic.services.RestaurantService;
import labTic.services.entities.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ManagerBuscarController {

    @Autowired
    RestaurantService restaurantService;

    @Autowired
    RestaurantRepository restaurantRepository;

    @FXML
    private TextField txtRUT;

    @FXML
    private Text txtNombre;

    @FXML
    private Text txtDireccion;

    @FXML
    private Text txtTelefono;

    @FXML
    private Text txtImporte;



    @FXML
    void btnBuscar(MouseEvent event) {
        try{
            Restaurant restaurant = restaurantRepository.findOneByRut(Long.valueOf(txtRUT.getText()));
            txtImporte.setText(""+restaurantService.getFee(restaurant));
            txtNombre.setText(restaurant.getName());
            txtDireccion.setText(restaurant.getAddress());
            txtTelefono.setText(restaurant.getPhone());
        }catch(Exception e ){
            e.printStackTrace();
        }

    }
    @FXML
    void createRestaurantTab(MouseEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setControllerFactory((ManagerMain.getContext()::getBean));

            Parent root = loader.load(ManagerController.class.getResourceAsStream("Manager/Manager.fxml"));

            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();

            stage.setScene(new Scene(root));
            stage.getScene().getStylesheets().add(ManagerController.class.getResource("Manager/Manager.css").toExternalForm());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @FXML
    void searchRestaurantTab(MouseEvent event) {

    }


}
