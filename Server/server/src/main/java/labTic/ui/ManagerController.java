package labTic.ui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import labTic.ClientMain;
import labTic.ManagerMain;
import labTic.RestaurantMain;
import labTic.services.RestaurantService;
import labTic.services.exceptions.InvalidRestaurantInformationException;
import labTic.services.exceptions.RestaurantAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ManagerController {

    @Autowired
    RestaurantService restaurantService;

    @FXML
    private TextField rut;

    @FXML
    private TextField address; //address

    @FXML
    private TextField area; //area

    @FXML
    private TextField name; //name

    @FXML
    private TextField style; //style

    @FXML
    private TextField phoneNumber;

    @FXML
    private TextField price;

    @FXML
    private TextField food;

    @FXML
    void createRestaurantTab(MouseEvent event) {

    }

    @FXML
    void searchRestaurantTab(MouseEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setControllerFactory((ManagerMain.getContext()::getBean));

            Parent root = loader.load(ManagerBuscarController.class.getResourceAsStream("Manager/ManagerBuscar.fxml"));

            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();

            stage.setScene(new Scene(root));
            stage.getScene().getStylesheets().add(ReservasController.class.getResource("Manager/ManagerBuscar.css").toExternalForm());
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    @FXML
    void createRestaurant(MouseEvent event) {
        String name = this.name.getText();
        String style = this.style.getText();
        String sPhoneNumber = phoneNumber.getText();
        String address = this.address.getText();
        String area = this.area.getText();
        String food = this.food.getText();
        String price = this.price.getText();

        //addClient(String firstName, String lastName, String email, String user, String password, String phoneNumber)
        try{
            long rut = Long.parseLong(this.rut.getText());
            restaurantService.addRestaurant(rut, name, address, style, sPhoneNumber, area, food, price);
            ClientMain.showAlert("Registro Exitoso","Restaurante Registrado Correctamente");
            clean();
        }catch(RestaurantAlreadyExistsException ras){
            ClientMain.showAlert("Error", "El Restaurante ya fue registrado");
        }catch(InvalidRestaurantInformationException iri){
            ClientMain.showAlert("Error", "No deje campos vacios");
        }catch(NumberFormatException nfe){
            ClientMain.showAlert("Error","El rut solo debe contener numeros");
        }
    }
    private void clean(){
        name.setText(null);
        address.setText(null);
        area.setText(null);
        style.setText(null);
        phoneNumber.setText(null);
        rut.setText(null);
        price.setText(null);
        food.setText(null);
    }
}
