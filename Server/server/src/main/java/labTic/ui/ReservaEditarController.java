package labTic.ui;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import labTic.RestaurantMain;
import labTic.services.RestaurantService;
import labTic.services.entities.Booking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReservaEditarController {

    @FXML
    private Text txtNombre;

    private Booking booking;

    @Autowired
    private RestaurantService restaurantService;

    @FXML
    private Text txtPersonas;

    @FXML
    private Text txtMesa;

    public void setBooking(Booking booking){
        this.booking = booking;
        txtNombre.setText(booking.getAlias());
        txtPersonas.setText(""+booking.getAssistants());
        txtMesa.setText(""+booking.getTable().getId());
    }

    @FXML
    void btnFinalizar(MouseEvent event) {
        restaurantService.release(booking.getRestaurant(),booking.getAlias());
        goToReservas(event);
    }
    private void goToReservas(Event event){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setControllerFactory((RestaurantMain.getContext()::getBean));

            Parent root = loader.load(ReservasController.class.getResourceAsStream("Restaurant/Reservas.fxml"));
            ReservasController controller = loader.getController();
            controller.setRestaurant(booking.getRestaurant());

            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();

            stage.setScene(new Scene(root));
            stage.getScene().getStylesheets().add(ReservasController.class.getResource("Restaurant/Reservas.css").toExternalForm());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @FXML
    void btnVolver(MouseEvent event) {
        goToReservas(event);

    }

    @FXML
    void perfilTab(MouseEvent event) {

    }

    @FXML
    void reservasTab(MouseEvent event) {

    }

}
