package labTic.ui;

import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import labTic.ClientMain;
import labTic.persistence.BookingRepository;
import labTic.services.RestaurantService;
import labTic.services.entities.Booking;
import labTic.services.entities.Client;
import labTic.services.entities.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;


@Component
public class ReservaPendienteController implements Initializable {

    private Restaurant restaurant;

    private Client client;

    private boolean cancelar = false;

    private Booking booking;

    private String alias;

    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    RestaurantService restaurantService;

    private Event event;

    @FXML
    private ImageView imgRestaurant;

    @FXML
    private ImageView imgLoading;

    @FXML
    private Text txtRestaurant;

    @FXML
    private Text txtUbicacion;

    @FXML
    private Text txtPersonas;
    @FXML
    private Text txtTelefono;

    @FXML
    private Text txtDireccion;

    public void setRestaurantAndClient(Restaurant restaurant, Client client, int cantPersonas, Event event,String alias) {
        this.event = event;
        this.client = client;
        this.restaurant = restaurant;
        this.restaurant = restaurant;
        this.alias = alias;
        txtPersonas.setText(""+cantPersonas);
        txtDireccion.setText(restaurant.getAddress());
        txtRestaurant.setText(restaurant.getName());
        txtUbicacion.setText(restaurant.getArea());
        txtTelefono.setText(restaurant.getPhone());
        try{
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(restaurant.getProfilePicture()));
            Image image = SwingFXUtils.toFXImage(img, null);
            imgRestaurant.setImage(image);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    void goBackToRestaurantView(Event event){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setControllerFactory((ClientMain.getContext()::getBean));

            Parent root = loader.load(RestaurantViewController.class.getResourceAsStream("Client/RestaurantView.fxml"));
            RestaurantViewController controller = loader.getController();
            controller.setRestaurant(restaurant);
            controller.setClient(client);

            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();

            stage.setScene(new Scene(root));
            stage.getScene().getStylesheets().add(RestaurantViewController.class.getResource("Client/RestaurantView.css").toExternalForm());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    void goBackToRestaurantView1(Event event){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setControllerFactory((ClientMain.getContext()::getBean));

            Parent root = loader.load(RestaurantViewController.class.getResourceAsStream("Client/RestaurantView.fxml"));
            RestaurantViewController controller = loader.getController();
            controller.setRestaurant(restaurant);
            controller.setClient(client);

            Stage stage = new Stage();

            stage.setScene(new Scene(root));
            stage.getScene().getStylesheets().add(RestaurantViewController.class.getResource("Client/RestaurantView.css").toExternalForm());
            stage.showAndWait();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    void goToReservaConfirmadaController(Event event){
        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setControllerFactory((ClientMain.getContext()::getBean));

            Parent root = loader.load(ReservaConfirmadaController.class.getResourceAsStream("Client/ReservaConfirmada.fxml"));
            ReservaConfirmadaController controller = loader.getController();
            controller.setRestaurant(restaurant);
            controller.setClient(client);
            booking = restaurantService.getClientBookingsOnHold(alias);
            controller.setBooking(booking);

            Stage stage = new Stage();

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.getScene().getStylesheets().add(ReservaConfirmadaController.class.getResource("Client/RestaurantView.css").toExternalForm());
            stage.showAndWait();
            Platform.exit();
        }catch (Exception e){
            e.printStackTrace();
        }

    }



    @FXML
    void btnCancelar(MouseEvent event) {
        booking.setRejected();
        bookingRepository.save(booking);
        cancelar = true;
        ClientMain.showAlert("Cancelado","Reserva Cancelada");
        goBackToRestaurantView(event);

    }

    @FXML
    void btnVolver(MouseEvent event) {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        long startTime = System.currentTimeMillis();

        ClassPathResource backImgFile = new ClassPathResource("labTic/ui/Client/images/loading.gif");
        Image image = new Image("labTic/ui/Client/images/loading.gif");
        imgLoading.setImage(image);



        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            public void run(){
                long thisTime = System.currentTimeMillis();
                booking = restaurantService.getClientBookingsOnHold(alias); //Aca va la funcion que devuelve la reserva de la base de datos

                if(booking!=null){
                if(booking.getConfirmed() == true){
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            timer.cancel();
                            showAlert("Exito","Reserva Confirmada");
                            goToReservaConfirmadaController(event);

                        }
                    });


                }
                if( thisTime-startTime > 600000 ){
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            booking.setRejected();
                            bookingRepository.save(booking);
                            showAlert("Error","La reserva no ha sido confirmada");
                            goBackToRestaurantView1(event);
                            timer.cancel();
                        }
                    });

                }
                if(cancelar == true){
                    timer.cancel();

                }
            }else if(cancelar==false){
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            timer.cancel();
                            showAlert("Error","Reserva rechazada por el restaurante");
                            goBackToRestaurantView1(event);
                        }
                    });

                }
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0, 5000);

    }
    public void showAlert(String title, String contextText) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(contextText);
        alert.showAndWait();

    }
}
